
class CheckOut

  def initialize rules
#Lista de regras de preços disponíveis
    item = ''
    @prices = {} #hash de promoções de preços
    
    rules.each do |line|
      if line[0..0] == "\t"
        quantity, total = line[1..-1].split "\t"
        @prices[item] = {} if @prices[item].nil?
        @prices[item][quantity.to_i] = total.to_i #O método to_i devolve o nЩmero que é formado por todos os dьgitos no inicio de uma cadeia de caracteres .
      else item = line
      end
    end
    @items = {} # Se um item não for encontrado, a quantidade é zero.
  end

# Incremento do contador de itens.
  def scan item
    @items[item] = 0 if @items[item].nil?
    @items[item] += 1
  end

# Calcula total geral pela soma total de cada produto
  def total
    total = 0
    @items.each_pair do |item, quantity|
      while quantity > 0 do
        rule_quantity, rule_price = best_rule item, quantity
        num_rules = quantity / rule_quantity
        total += num_rules * rule_price
        quantity -= num_rules * rule_quantity
      end
    end
    total
  end

  private

 #Calcula total de um item, utilizando a melhor regra disponível para a quantidade.
  def best_rule item, quantity
    rule = nil
    @prices[item].each_pair do |rule_quantity, rule_total|
      if rule_quantity > quantity then
        break
      else

        rule= [rule_quantity, rule_total]
      end
    end
    puts "Não foi encontrado um Preço promocional para #{quantity} itens do produto #{item}!" if rule.nil?
    rule
  end

end


RULES = [
  "A",
  "\t1\t50",
  "\t3\t130",
  "B",
  "\t1\t30",
  "\t2\t45",
  "C",
  "\t1\t20",
  "D",
  "\t1\t15",
]
