#language: pt
Funcionalidade: Marcação de Mensagens

Para encontrar recados importantes rapidamente 
Como um usuário cadastrado
Eu quero ter recursos de marcação de mensagens


Cenário: Marcar uma mensagem

Dado que estou na tela de conversa de um Contato
Quando toco e seguro sobre uma mensagem 
  E toco no ícone de Estrela
  E retorno para a tela de Conversas
  E seleciono a opção "Mensagens Marcadas"	
Então devo ver uma lista com todas as mensagens marcadas
  E devo ver para cada mensagem marcada, a identificação do contato e a data de envio da mensagem


Cenário: Desmarcar uma mensagem

Dado que estou na tela de conversa de um Contato que possui uma mensagem sinalizada com um ícone de Estrela
Quando toco e seguro sobre esta mensagem 
  E toco no ícone de Remover Estrela
  E retorno para a tela de Conversas
  E seleciono a opção "Mensagens Marcadas"	
Então a mensagem desmarcada não é exibida na lista de Mensagens Marcadas.
