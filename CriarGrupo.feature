#language: pt
Funcionalidade: Criação de Grupos

Para realizar a troca de mensagens simultaneamente com vários contatos
Como um usuário cadastrado
Eu quero ter recursos de Conversas em Grupo


Cenário: Criando um Grupo

Dado que estou na tela de Conversas
Quando seleciono a opção "Novo Grupo"
  E preencho o nome do grupo com "Grupo Teste"
  E adiciono um participante ao grupo
  E seleciono a opção Criar
Então devo ver a tela de Conversas
  E devo ver no topo da lista de conversas o novo grupo com a mensagem "Você criou o grupo Grupo Teste"


Cenário: Adicionando Novo Participante

Dado que sou o administrador de um grupo existente
  E que estou na tela de conversa deste grupo 	
Quando toco no nome do grupo
  E seleciono a opção "Adicionar Participante"
  E escolho o contato "Contato Teste"
Então devo ver na lista de Participantes do grupo que o contato foi adicionado com sucesso
  E devo ver na tela da conversa do grupo a mensagem "Você adicionou Contato Teste"


Cenário: Removendo Participante

Dado que sou o administrador de um grupo existente
  E que estou na tela de conversa deste grupo 	
Quando toco no nome do grupo
  E toco sobre o nome de um Participante Contato Teste
  E seleciono a opção Remover
Então devo ver na lista de Participantes do grupo que o contato foi removido com sucesso
  E devo ver na tela da conversa do grupo a mensagem "Você removeu Contato Teste"
